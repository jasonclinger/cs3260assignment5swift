//
//  ViewController.swift
//  Assignment5
//
//  Created by Jason Clinger on 2/9/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

import UIKit
import MessageUI
//import Prephirences

class ViewController: UIViewController, UIPickerViewDelegate,  MFMailComposeViewControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    
    var array = [AnyObject]()
    //var array = [String]()
    
    
    //var items:[String] = ["row1", "row2", "row3"]
    var myTableView = UITableView()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //array = defaults("key")
        
        myTableView = UITableView(frame: self.view.bounds, style: .Plain)
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view!.addSubview(myTableView)
      
    } //end viewDidLoad
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        array = (defaults.objectForKey("expenseEntry") as! String).mutableCopy() as! [AnyObject]
        //array = defaults.objectForKey("expenseEntry")
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reloadData:", name: "reload", object: nil)
        
        //tableView.reloaddata
    }
    
        
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
        //return items.count
    }
        
    func reloadData(n: NSNotification) {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        array = (defaults.objectForKey("expenseEntry") as! String).mutableCopy() as! [AnyObject]
        //myTableView.reloadData()
        NSLog("Saving entry, reloading data")
    }
        
        
        
    //2nd func tableView, had to change name to avoid conflit
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell! = tableView.dequeueReusableCellWithIdentifier("cell")
        
        //var cell = tableView.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
            
        if (cell == nil)
        {
            cell = UITableViewCell(style: .Default, reuseIdentifier:"cell")
        }
            
        var dictionary: [NSObject:AnyObject] = array[indexPath.row] as! [String:String]
        cell!.textLabel?.text = (dictionary["location"] as! String)
        return cell!
        
        
    }
       
        //3rd func tableView, had to change name to avoid conflit
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
            
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
      self.performSegueWithIdentifier("rowTouched", sender: indexPath)
        
    }
        
    
    //optionally try UIStoryboardSegue!
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "rowTouched") {
            var ip: NSIndexPath = sender as! NSIndexPath
            var svc: secondViewController = segue.destinationViewController as! secondViewController
            //svc.info = array[ip.row] as! [NSDictionary]
            //svc.index = Int(ip.row) as! [NSNumber]
            
        }
    }
    
    func addItem(btn: UIBarButtonItem) {
        let svc: secondViewController = secondViewController()
        NSLog("addItem")
        self.navigationController!.pushViewController(svc, animated: true)
    }
    
    //@IBAction func addItem(sender: UIBarButtonItem) {
    //}
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

