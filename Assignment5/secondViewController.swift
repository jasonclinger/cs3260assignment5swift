//
//  secondViewController.swift
//  Assignment5
//
//  Created by Jason Clinger on 2/13/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

import UIKit
import MessageUI

class secondViewController: UIViewController, UIPickerViewDelegate,  MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var myDatePicker: UIDatePicker!
    
    @IBOutlet weak var amountTracker: UITextField!
    
    @IBOutlet weak var locationTracker: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.lightGrayColor()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnTouched(sender: UIButton) {
    
        printDate(myDatePicker.date)
        
        let mailComposeController = MFMailComposeViewController() //()news it up and allocates and initializes some memory
        mailComposeController.mailComposeDelegate = self;
        mailComposeController.setToRecipients(["me@me.com"]);
        mailComposeController.setSubject("test");
        mailComposeController.setMessageBody("you spent $ at this location", isHTML: false)
        //mailComposeController.setMessageBody("You spend" + amountTracker + " at" + locationTracker + " on" + myDatePicker.date, isHTML: false);
        //ask if can send mail
        if (MFMailComposeViewController.canSendMail()){
            self.presentViewController(mailComposeController, animated: true, completion: { () -> Void in
            })
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true) { () -> Void in
        }
    }
    
    func printDate(date :NSDate) {
        let dateFormatter = NSDateFormatter();
        
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle;
        dateFormatter.timeStyle = NSDateFormatterStyle.MediumStyle;
        
        NSLog("%@", dateFormatter.stringFromDate(date));
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.myDatePicker.date = ("date" as! NSDate)
        self.amountTracker.text = ("amount")
        self.locationTracker.text = ("location")
        
//        self.myDatePicker.date = (self.info["date"] as! NSDate)
//        self.amountTracker.text = (self.info["amount"] as! String)
//        self.locationTracker.text = (self.info["location"] as! String)
    }
    

    @IBAction func saveBtnTouched(sender: UIButton) {
        
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var dictionary = [myDatePicker : "date", locationTracker : "location", amountTracker : "amount"]
        
        var a = defaults.objectForKey("expenseEntry")
        
        if ((a == nil))
        {
            a = (defaults.objectForKey("expenseEntry") as! String).mutableCopy() as! [AnyObject]
        }
        
        a!.addObject(dictionary)
        
        defaults.setObject(a, forKey: "expenseEntry")
        defaults.synchronize()
        
        NSLog("updating entry")
        NSNotificationCenter.defaultCenter().postNotificationName("reload", object: nil)
        self.navigationController!.popViewControllerAnimated(true)
    
    }
  

}
